<?php
require_once __DIR__.'/../vendor/autoload.php';

$config = [
    'name' => 'basic',
    'method' => 'GET',
    'fields' => [
        'login' => [
            'type' => 'email',
            'label' => 'Your email address',
            'required' => true,
        ],
        'password' => [
            'type' => 'password',
            'required' => true,
        ],
    ],
    'submit_text' => 'log in',
];

error_reporting(E_ALL);
$form = new \Fu\Form($config);

if ($form->isSubmitted()) {


}

?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Basic example</title>

    <style>
        .control.error, .errors {
            color: red;
        }

        .success {
            color: green;
        }
    </style>
</head>
<body>

<h1>Basic example</h1>

<?php
if ($form->isSubmitted()) {
    echo '<p>The form was submitted.</p>';

    $errors = [];
    if ($form->isValid()) {
        echo '<p class="success">The form is valid, process it!.</p>';
    } else {
        $errors = $form->getErrors();
        printf('<div class="errors"><p>Uh oh:</p><ul><li>%s</li></ul></div>', implode('</li><li>', $errors));
    }
}

$form->display();
?>


</body>
</html>
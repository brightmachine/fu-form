Fu\Form
=====

A wrapper around pear's HTML QuickForm library, although we use [a slightly more up to date fork](https://github.com/flack/quickform).

## Installation

In your composer.json file, add the following…

    "repositories": [
        {"type": "vcs", "url":  "git@bitbucket.org:brightmachine/fu-form.git"}
    ],
    "require": {
        "brightmachine/fu-form": "dev-master"
    }

## Example

To get an idea of how you can use `Fu\Form` run the examples locally:

    php -S 0.0.0.0:8000 -t examples/
    

## Custom rendering

In order to render forms how you want, you can setup your own Renderer.

Do this by extending `Fu\Form\Renderer` and tweaking until you get things working.